#!/usr/bin/env python3
# 
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2018 Rafal Kozik
# All rights reserved.
#
import sys
import time


class Mcp2221:
    def __init__(self, d):
        self.SIZE = 64
        self.INIT = bytearray([0x60, 0x00, 0x00, 0x00,
                               0x00, 0x00, 0x00, 0x80,
                               0x10, 0x10, 0x02, 0x10])
        self.SET_SIZE = 18
        self.SET_START = 0x50
        self.ADC_SIZE = 5
        self.ADC_START = 0x10
        self.d = d
        out = self.send_command(self.INIT)
        assert out[0] == 0x60 and out[1] == 0x00,\
            "Cannot init device"

    def send_command(self, cmd):
        self.d.write(cmd)
        return self.d.read(self.SIZE)

    def set_led(self, color, state):
        to_send = bytearray(self.SET_SIZE)
        to_send[0] = self.SET_START
        if color == "green":
            n = 2
        elif color == "blue":
            n = 6
        else:
            n = 14
        to_send[n] = 0x01
        to_send[n+1] = state
        self.send_command(to_send)

    def get_adc(self):
        to_send = bytearray(self.ADC_SIZE)
        to_send[0] = self.ADC_START
        out = self.send_command(to_send)
        return out[53]*256+out[52]


if __name__ == "__main__":
    assert len(sys.argv) >= 2, "Provide HID device"
    print("Opening {}".format(sys.argv[1]))
    with open(sys.argv[1], "w+b", buffering=0) as d:
        mcp = Mcp2221(d)
        while 1:
            mcp.set_led("green", 0)
            time.sleep(1)
            mcp.set_led("green", 1)
            time.sleep(1)
