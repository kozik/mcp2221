#!/usr/bin/env python3
#
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2018 Rafal Kozik
# All rights reserved.
#
from tkinter import *
from mcp2221 import Mcp2221


class Led:
    def __init__(self, mcp, color):
        self.mcp = mcp
        self.color = color
        self.state = 1

    def __call__(self):
        self.state = 1 - self.state
        mcp.set_led(self.color, self.state)


class Pot:
    def __init__(self, mcp, root):
        self.mcp = mcp
        self.root = root
        self.s = Scale(from_=0, to=1023, orient=HORIZONTAL)
        self.s.pack()
        self.update()

    def update(self):
        self.s.set(mcp.get_adc())
        self.root.after(200, self.update)


if __name__ == "__main__":
    assert len(sys.argv) >= 2, "Provide HID device"
    print("Opening {}".format(sys.argv[1]))
    with open(sys.argv[1], "w+b", buffering=0) as d:
        mcp = Mcp2221(d)
        root = Tk()
        root.title("MCP")
        for c in ["red", "green", "blue"]:
            b = Button(command=Led(mcp, c), bg=c).pack()
        p = Pot(mcp, root)
        root.mainloop()
